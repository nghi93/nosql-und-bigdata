package test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

public class TwitterCrawlerTest {
	private String TABLENAME = "Meow";
	private String FAMILYNAME = "Twitter";
	
	@Test
	public void test() {
		Configuration config = HBaseConfiguration.create();
		try {
			HTable table = new HTable(config, TABLENAME);
			Scan scan = new Scan(); 
			scan.setFilter(new SingleColumnValueFilter(Bytes.toBytes(FAMILYNAME), Bytes.toBytes("movie"), CompareOp.EQUAL, Bytes.toBytes("The Imitation Game")));
			ResultScanner rs = table.getScanner(scan);
            for (Result r : rs) { 
                System.out.println("key:" + new String(r.getRow())); 
                System.out.println("tweet"+new String(r.getValue(Bytes.toBytes(FAMILYNAME), Bytes.toBytes("tweet"))));
                System.out.println("created"+Bytes.toLong(r.getValue(Bytes.toBytes(FAMILYNAME), Bytes.toBytes("created"))));
                byte[] lat = r.getValue(Bytes.toBytes(FAMILYNAME), Bytes.toBytes("latitude"));
                if(lat != null){
                	System.out.println("latitude"+Bytes.toDouble(lat));
                }
                byte[] longi = r.getValue(Bytes.toBytes(FAMILYNAME), Bytes.toBytes("longitude"));
                if(longi != null){
                	System.out.println("longitude"+Bytes.toDouble(longi));
                }
            }  
            rs.close();  
            table.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
