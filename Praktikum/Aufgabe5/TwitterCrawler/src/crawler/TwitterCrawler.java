package crawler;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterCrawler {
	ConfigurationBuilder cb;
	int LIMIT= 500;
	Twitter twitter;
	private String oAuthConsumerKey = "jF6IEgqwkuPwWYykD0zgwCZOM";
	private String oAuthConsumerSecret = "1HiX5PPcFWNaGmcCcWaBdet6tDY0sczkFukRErtYWaE2L6PmYC";
	private String oAuthAccessToken = "2889430695-BvKsQobpwyXtRheFs2vdE0dCvhntbtFvCQRz8HS";
	private String oAuthAccessTokenSecret = "pzpddaoNduSTwJLok8qHrPg5cjp4lQPsGSqme1vX8hEQ4";
	private String TABLENAME = "Meow";
	private String FAMILYNAME = "Twitter";
	
	
	public static void main(String[] args){
		new TwitterCrawler().run();
	}
	
	public TwitterCrawler(){
		cb = new ConfigurationBuilder();
		cb.setOAuthConsumerKey(oAuthConsumerKey);
		cb.setOAuthConsumerSecret(oAuthConsumerSecret);
		cb.setOAuthAccessToken(oAuthAccessToken);
		cb.setOAuthAccessTokenSecret(oAuthAccessTokenSecret);
		twitter = new TwitterFactory(cb.build()).getInstance();
	}
	
	public void run(){
		Configuration config = HBaseConfiguration.create();
		
		
		
		Query query = new Query("The Imitation Game");
		query.setCount(5);
		try {
			HTable table = new HTable(config, TABLENAME);
			
			QueryResult r = twitter.search(query);
			String TWEET_QUALIFIER = "tweet";
			String LATITUDE_QUALIFIER = "latitude";
			String LONGITUDE_QUALIFIER = "longitude";
			String CREATED_QUALIFIER = "created";
			String MOVIE_QUALIFIER = "movie";
			Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			for(Status s : r.getTweets()){
				Put p = new Put(Bytes.toBytes(String.valueOf(s.getId())));
				
				
				p.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(TWEET_QUALIFIER), Bytes.toBytes(s.getText()));
				if(s.getGeoLocation() != null){
					p.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(LATITUDE_QUALIFIER), Bytes.toBytes(s.getGeoLocation().getLatitude()));
					p.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(LONGITUDE_QUALIFIER), Bytes.toBytes(s.getGeoLocation().getLongitude()));
				}
				p.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(CREATED_QUALIFIER), Bytes.toBytes(formatter.format(s.getCreatedAt())));
				p.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(MOVIE_QUALIFIER), Bytes.toBytes("The Imitation Game"));
				table.put(p);
				
			}
			
			table.close();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
