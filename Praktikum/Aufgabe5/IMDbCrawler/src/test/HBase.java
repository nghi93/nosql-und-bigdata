package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

public class HBase {
	private final String TABLENAME = "Meow";
	private final byte[] CF = Bytes.toBytes("IMDb");
	@Test
	public void testHBase() {
		Configuration config = HBaseConfiguration.create();
		try {
			HTable table = new HTable(config, TABLENAME);
			Scan scan = new Scan();
			scan.addFamily(CF);
			scan.addColumn(CF, Bytes.toBytes("1"));
			ResultScanner rs = table.getScanner(scan);
			Iterator<Result> it = rs.iterator();
			while(it.hasNext()){
				String s = new String(it.next().getValue(CF, Bytes.toBytes("1")));
				System.out.println(s);
			}
			table.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
