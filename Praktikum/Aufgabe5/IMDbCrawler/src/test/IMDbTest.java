package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import crawler.IMDbPage;
import crawler.Page;

public class IMDbTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetCasts() {
		IMDbPage imdb = new IMDbPage("http://www.imdb.com/title/tt2084970/");
		List<String> expected = new ArrayList<String>(Arrays.asList("Benedict Cumberbatch", "Keira Knightley", "Matthew Goode", "Rory Kinnear", "Allen Leech", "Matthew Beard", "Charles Dance", "Mark Strong", "James Northcote", "Tom Goodman-Hill", "Steven Waddington", "Jack Tarlton", "Alex Lawther", "Jack Bannon", "Tuppence Middleton"));
		assertEquals(expected, imdb.getCasts());
	}
	
	@Test
	public void testGetRatings(){
		IMDbPage imdb = new IMDbPage("http://www.imdb.com/title/tt2084970/");
		List<String> result = imdb.getRatings();
//		for(String s : result){
//			System.out.println(s);
//		}
		System.out.println(result.size());
	}

	@Test
	public void testGetRatingInNumber(){
		IMDbPage imdb = new IMDbPage("http://www.imdb.com/title/tt1853728/");
		System.out.println(imdb.getRatingNumber());
	}
	
	@Test
	public void testGetTitle(){
		IMDbPage imdb = new IMDbPage("http://www.imdb.com/title/tt1853728/");
		assertEquals("Django Unchained", imdb.getTitle());
	}
	
	@Test
	public void testPublicationDate(){
		IMDbPage imdb = new IMDbPage("http://www.imdb.com/title/tt1853728/");
		assertEquals("(2012)", imdb.getPublicationDate());
	}
	
	@Test
	public void testGetDates(){
		IMDbPage imdb = new IMDbPage("http://www.imdb.com/title/tt2084970/");
		List<String> result = imdb.getCreationDate();
		System.out.println(result);
	}
	
	@Test
	public void testGetGeo(){
		IMDbPage imdb = new IMDbPage("http://www.imdb.com/title/tt2084970/");
		imdb.getRatings();
		List<String> result = imdb.getGeo();
		System.out.println(result.size());
	}
}
