package crawler;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class IMDbPage {
	private String url;
	
	/**
	 * 
	 * @param page must be main page of the movie, e.g.
	 * http://www.imdb.com/title/tt2084970/
	 */
	public IMDbPage(String url){
		this.url = url;
	}
	
	public String getTitle(){
		Page page;
		try {
			page = new Page(url);
			return page.getDocument().select(".header > .itemprop").text();
		} catch (IOException e) {
			return "";
		}
	}

	public String getPublicationDate(){
		Page page;
		try {
			page = new Page(url);
			return page.getDocument().select(".header > .nobr").text();
		} catch (IOException e) {
			return "";
		}
	}
	
	/**
	 * 
	 * @return casts
	 */
	public List<String> getCasts(){
		List<String> result = new ArrayList<String>();
		
		try {
			Page page = new Page(url);
			Elements e = page.getDocument().select(".cast_list td .itemprop");
			Iterator<Element> it = e.iterator();
			while(it.hasNext()){
				result.add(it.next().text());
			}
			return result;
		} catch (IOException e1) {
			return result;
		}

	}
	
	public List<String> getCreationDate(){
		return getCreationDateForPage(0, new ArrayList<String>());
	}
	
	private List<String> getCreationDateForPage(int page, List<String> accu){
		int i = (page-1) * 10;
		String reviewURL = url+"reviews?start="+i;
		SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
		String date = "";
		try {
			Page p = new Page(reviewURL);
			Elements e2 = p.getDocument().select("#tn15content div").not("[class]");
			if(e2.size() == 0){
				return accu;
			}
			Iterator<Element> it = e2.iterator();
			while(it.hasNext()){
				Elements small = it.next().select("small");
				if(small.size() == 3){
					date = small.get(2).text();
				}
				else {
					date = small.get(1).text();
				}
				accu.add(date);
			}
			return getCreationDateForPage(page+1, accu);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
			 
	}
	
	public List<String> getGeo(){
		return getGeoForPage(0, new ArrayList<String>());
	}
	
	private List<String> getGeoForPage(int page, List<String> accu){
		int i = (page-1) * 10;
		String reviewURL = url+"reviews?start="+i;
		try {
			Page p = new Page(reviewURL);
			Elements e2 = p.getDocument().select("#tn15content div").not("[class]");
			if(e2.size() == 0){
				return accu;
			}
			Iterator<Element> it = e2.iterator();
			while(it.hasNext()){
				Elements small = it.next().select("small");
				if(small.size() == 3){
					accu.add(small.get(1).text());
				}
				else{
					accu.add(null);
				}
			}
			return getGeoForPage(page+1, accu);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public List<String> getRatings(){
		return getRatingForPage(0, new ArrayList<String>());
	}
	
	private List<String> getRatingForPage(int page, List<String> accu){
		int i = (page-1) * 10;
		String reviewURL = url+"reviews?start="+i;
		try {
			Page p = new Page(reviewURL);
			Elements e = p.getDocument().select("#tn15content div + p");
			//e.remove(e.size()-1);
			
			//termination condition
			if(e.size() == 0){
				return accu;
			}
			Iterator<Element> it = e.iterator();
			while(it.hasNext()){
				accu.add(it.next().text());
				
			}
			return getRatingForPage(page+1, accu);
		} catch (IOException e) {
			return null;
		} 
	}
	
	public double getRatingNumber(){
		return getRatingNumberForPage(0, 0.0, 0.0);
	}
	
	private double getRatingNumberForPage(int page, double accu, double accu2){
		int i = (page-1) * 10;
		String reviewURL = url+"reviews?start="+i;
		try {
			Page p = new Page(reviewURL);
			Iterator<Element> it = p.getDocument().select("#tn15content img[width=102]").iterator();
			if(!it.hasNext()){
				return accu/accu2;
			}
			while(it.hasNext()){
				accu = accu+Integer.parseInt(it.next().attr("alt").split("/")[0]);
				accu2++;
			}
			return getRatingNumberForPage(page+1, accu, accu2);
		} catch (IOException e) {
			return 0;
		}
	}
}
