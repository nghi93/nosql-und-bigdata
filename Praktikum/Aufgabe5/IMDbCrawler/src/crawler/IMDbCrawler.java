package crawler;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

public class IMDbCrawler {
	private String url;
	private final String TABLENAME = "Meow";
	private final String FAMILIYNAME = "IMDb";
	
	public static void main(String[] args){
		if(args.length < 1){
			System.out.println("IMDbCrawler <URL>");
			System.exit(-1);
		}
		new IMDbCrawler(args[0]).run();
	}
	
	public IMDbCrawler(String url){
		this.url = url;
	}
	
	public void run(){
		IMDbPage page = new IMDbPage(url);
		page.getRatings();
		
		Configuration config = HBaseConfiguration.create();
		try {
			HTable table = new HTable(config, TABLENAME);
			Put p = new Put(Bytes.toBytes(page.getTitle()+page.getPublicationDate()));
			int i = 0;
			String REVIEW = "review";
			String CREATED = "created";
			String LOCATION = "location";
			List<String> ratings = page.getRatings();
			List<String> geo = page.getGeo();
			List<String> dates = page.getCreationDate();

			for(String s : ratings){
				p.add(Bytes.toBytes(FAMILIYNAME), Bytes.toBytes(String.valueOf(i)+REVIEW), Bytes.toBytes(s));
				if(geo.get(i) != null){
					p.add(Bytes.toBytes(FAMILIYNAME), Bytes.toBytes(String.valueOf(i)+LOCATION), Bytes.toBytes(geo.get(i)));
				}
				p.add(Bytes.toBytes(FAMILIYNAME), Bytes.toBytes(String.valueOf(i)+CREATED), Bytes.toBytes(dates.get(i).toString()));
				i++;
			}
			table.put(p);
			table.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}