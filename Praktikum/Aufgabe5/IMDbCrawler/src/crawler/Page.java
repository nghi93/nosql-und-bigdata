package crawler;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Page {
	private static final int DEFAULT_TIMEOUT_IN_MILLISEC = 10*1000;
	private static final int DEFAULT_NUMBER_OF_RETRY = 3;
	
	private Document html;
	private String url;
	/**
	 * Connects to a URL within certain milliseconds, otherwise tries again
	 * @param URL
	 * @param millis time till timeout
	 * @param retry number of trial
	 * @throws IOException when couldn't connect
	 */
	public Page(String URL, int millis, int retry) throws IOException{
		int count = 0;
		this.url = URL;
		try{
			this.html = Jsoup.connect(URL).timeout(millis).get();
		}
		catch(IOException e){
			if (++count == retry) throw e;
		}
	}
	
	public Page(String URL) throws IOException{
		this(URL, DEFAULT_TIMEOUT_IN_MILLISEC);
	}
	
	public Page(String URL, int millis) throws IOException{
		this(URL, millis, DEFAULT_NUMBER_OF_RETRY);
	}
	
	public Document getDocument(){
		return html;
	}
	
	public String getURL(){
		return url;
	}
}
