package sentiment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Iterator;

import com.aliasi.classify.Classification;
import com.aliasi.classify.ConditionalClassification;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassifier;
import com.aliasi.classify.LMClassifier;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.BoundedPriorityQueue;
import com.aliasi.util.ScoredObject;

public class IMDbSentimentHierarchicalPolarityClassifier {
    static int MIN_SENTS = 5;
    static int MAX_SENTS = 25;

	private String MODEL = "polarity.model";
	private JointClassifier<CharSequence> mSubjectivityClassifier;
	private     DynamicLMClassifier<NGramProcessLM> mClassifier;
	
	String[] categories; 
	LMClassifier clazz; 
	public IMDbSentimentHierarchicalPolarityClassifier() { 
		try { 
	        int nGram = 8;
	        mClassifier 
	            = DynamicLMClassifier
	              .createNGramProcess(new String[]{"neg", "pos"},nGram);
	        
	        File modelFile = new File("subjectivity.model");
	        FileInputStream fileIn = new FileInputStream(modelFile);
	        ObjectInputStream objIn = new ObjectInputStream(fileIn);
	        @SuppressWarnings("unchecked")
	        JointClassifier<CharSequence> subjectivityClassifier
	            = (JointClassifier<CharSequence>) objIn.readObject();
	        mSubjectivityClassifier = subjectivityClassifier;
	        objIn.close();
			
			clazz = (LMClassifier) AbstractExternalizable.readObject(new File(MODEL)); 
			categories = clazz.categories(); 
		} 
		catch (ClassNotFoundException e) { 
			e.printStackTrace(); 
		} 
		catch (IOException e) { 
			e.printStackTrace(); 
		} 
	} 
	
	public String classify(String text) { 
        String subjReview = subjectiveSentences(text);
        Classification classification
            = mClassifier.classify(subjReview);

		return classification.bestCategory(); 
	} 
	
    String subjectiveSentences(String review) {
        String[] sentences = review.split("\n");
        BoundedPriorityQueue<ScoredObject<String>> pQueue 
            = new BoundedPriorityQueue<ScoredObject<String>>(ScoredObject.comparator(),
                                                             MAX_SENTS);
        for (int i = 0; i < sentences.length; ++i) {
            String sentence = sentences[i];
            ConditionalClassification subjClassification
                = (ConditionalClassification) 
                mSubjectivityClassifier.classify(sentences[i]);
            double subjProb;
            if (subjClassification.category(0).equals("quote"))
                subjProb = subjClassification.conditionalProbability(0);
            else
                subjProb = subjClassification.conditionalProbability(1);
            pQueue.offer(new ScoredObject<String>(sentence,subjProb));
        }
        StringBuilder reviewBuf = new StringBuilder();
        Iterator<ScoredObject<String>> it = pQueue.iterator();
        for (int i = 0; it.hasNext(); ++i) {
            ScoredObject<String> so = it.next();
            if (so.score() < .5 && i >= MIN_SENTS) break;
            reviewBuf.append(so.getObject() + "\n");
        }
        String result = reviewBuf.toString().trim();
        return result;
    }
}
