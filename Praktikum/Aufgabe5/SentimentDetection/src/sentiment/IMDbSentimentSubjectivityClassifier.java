package sentiment;

import java.io.File;
import java.io.IOException;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.ConditionalClassification;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassifierEvaluator;
import com.aliasi.classify.LMClassifier;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.util.AbstractExternalizable;

public class IMDbSentimentSubjectivityClassifier { 
	private String MODEL = "subjectivity.model";
	String[] categories; 
	LMClassifier clazz; 
	public IMDbSentimentSubjectivityClassifier() { 
		try { 
			clazz = (LMClassifier) AbstractExternalizable.readObject(new File(MODEL)); 
			categories = clazz.categories(); 
		} 
		catch (ClassNotFoundException e) { 
			e.printStackTrace(); 
		} 
		catch (IOException e) { 
			e.printStackTrace(); 
		} 
	} 
	
	public String classify(String text) { 
		String[] mCategories = new String[] { "plot", "quote" };
        int nGram = 8;
        DynamicLMClassifier<NGramProcessLM> mClassifier = 
            DynamicLMClassifier
            .createNGramProcess(mCategories,nGram);
    
        boolean storeInputs = false;
        JointClassifierEvaluator<CharSequence> evaluator
            = new JointClassifierEvaluator<CharSequence>(mClassifier, mCategories,storeInputs);
		
		for (int i = 0; i < mCategories.length; ++i) {
		 String category = mCategories[i];
         Classification classification
             = new Classification(category);
         Classified<CharSequence> classified
         = new Classified<CharSequence>(text,classification);
         evaluator.handle(classified);
		}
		return evaluator.toString();
//		ConditionalClassification classification = clazz.classify(text);
//		return classification.bestCategory();
	} 
}