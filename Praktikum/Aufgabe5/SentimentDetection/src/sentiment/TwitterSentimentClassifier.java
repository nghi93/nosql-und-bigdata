package sentiment;

import java.io.File;
import java.io.IOException;

import com.aliasi.classify.ConditionalClassification;
import com.aliasi.classify.LMClassifier;
import com.aliasi.util.AbstractExternalizable;

public class TwitterSentimentClassifier { 
	private String MODEL = "/home/nosql/Dokumente/Uni/nosql-und-bigdata/Praktikum/Aufgabe5/Twitter_lib/twitter.model";
	String[] categories; 
	LMClassifier clazz; 
	public TwitterSentimentClassifier() { 
		try { 
			clazz = (LMClassifier) AbstractExternalizable.readObject(new File(MODEL)); 
			categories = clazz.categories(); 
		} 
		catch (ClassNotFoundException e) { 
			e.printStackTrace(); 
		} 
		catch (IOException e) { 
			e.printStackTrace(); 
		} 
	} 
	
	public String classify(String text) { 
		ConditionalClassification classification = clazz.classify(text); 
		return classification.bestCategory(); 
	} 
}