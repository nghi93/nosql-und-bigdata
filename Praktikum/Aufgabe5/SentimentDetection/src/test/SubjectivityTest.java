package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sentiment.IMDbSentimentPolarityClassifier;
import sentiment.IMDbSentimentSubjectivityClassifier;

public class SubjectivityTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSubjectivity() {
		String result = new IMDbSentimentSubjectivityClassifier().classify("It took a little over 24 hours before I weighed in on my official thoughts on Morten Tyldum's The Imitation Game from The Weinstein Company. My initial reaction upon leaving the screening room was it was astonishing, a magnificent achievement that stands tall as one of the year's best movies. As the film continues to settle within my cinematic soul, this very well could be the best film of the year, anchored by a career best performance from the amazing Benedict Cumberbatch. Full disclosure, I'm fairly oblivious to European history and the heroes that had a hand in one of the deadliest wars in history. I've heard the name Alan Turing from high school and college but either didn't care enough to learn or have no recollection of his contributions. Minutes following the screening, Amazon.com got $15.82 from my bank account in order to read \"Alan Turing: The Enigma,\" the book in which screenwriter Graham Moore based the story upon. Telling the story of Alan Turing, a mathematician who in 1939 led a pioneer in cracking one of the most difficult codes in history. His contributions paved the way for essentially the way we exist now. However, Turing, who is a homosexual, has to wrestle with his secret in order to keep his status and his work years later. Masterfully told and encompassing an emotional complexity, Tyldum's film is both engrossing and disturbing. It has genius aspirations in which it wants to exist in the cinematic world. It's an impeccable thriller, taut and brilliant, exploring the horrors of war along with the choices that doom mankind for all eternity. Tyldum is methodical and precise in which he decides to unravel the story, Turing is one of the fallen heroes of our history and his story stands as one of the most tragic. Screenwriter Moore crafts a murky, dark, yet totally enjoyable spy film that stands taller than any James Bond film ever released. It's a sure-fire Oscar contender for several Academy Awards including Best Picture. They should feel so lucky to have the gumption to choose something this methodical and majestic. Benedict Cumberbatch continues to climb the ladder as one of the best actors working today. After impressive performances August: Osage County, 12 Years a Slave, and TV's \"Sherlock,\" this is the role that will make him a bonafide movie star. Oscar-winner or not, this will be looked upon like the greats such as Gene Hackman in The French Connection or any legendary 70's movie that you love today. Cumberbatch hones in on all of Turing's character flaws and good qualities that make him a real person. He constructs him from the toes up, inflicting mannerisms and behaviors that all ring true. He stimulates all the sensual beats that keep us fixated on a performance. I can't help but go back to someone like Joaquin Phoenix in The Master, who delivered a construction of epic proportions. Though based on a real person, the talented Cumberbatch ignites his own masterpiece performance. He follows the demons of Turing down to his bones. Unsure, arrogant, and dismissive to the world around him, Turing shows only what he must, what he chooses, and every once in a while, we get a front seat to his soul. Thank you Cumberbatch. The rest of the cast is completely on their game. It's probably a contender for the SAG Ensemble prize. Academy Award nominee Keira Knightley, as the feisty and fiery Joan Clark, is as loose and comfortable as I've ever seen her. She wears Joan like an old coat from the back of the closet. Remembering it fondly and seeing that it fits just perfect. She has all the things that make up an Oscar nominee; a scene that will likely bring you to tears, plenty of scenes that play as the comic relief in a dark tale, and being simply charming in every part of the film. I don't know when it's going to happen but the world needs to make Matthew Goode a mega-star. In his brief time on-screen, Goode makes his mark, becoming essentially a co-anchor with Knightley of the supporting players, showcasing a reason to give this guy his own leading role sooner rather than later. As our resident sleazy authority figure, Charles Dance shows that he's still got it. Mark Strong and Allen Leech also deliver memorable, fascinating scenes, both getting an opportunity to shine. Technical merits are no shortage of excellence on display. Oscar- winning Editor William Goldenberg (Argo) shows that tension is his second language. Cutting the film to perfection, and forcing your heart into throat, this espionage thriller succeeds for general audiences because of Goldenberg's efforts. It's something that anyone can seek out and get fully immersed into. Alexandre Desplat tacks another impressive composition to his already thick resume. With films like The Grand Budapest Hotel already in his arsenal, I assume this to be another Oscar citation in his future. Shot by the talented Oscar Faura, responsible for painting the canvas that was J.A. Bayona's The Impossible, he utilizes the standard brilliance of capturing a moment. Knows when to pull back and get close. Let's not forget the Production and Costume Design by Maria Djurkovic and Sammy Sheldon Differ. Those two will surely be mentioned for the rest of the film year. The Imitation Game is assertive and makes a serious claim as one of the best spy thrillers ever made. There are sub plots that all resonate and never feel forced. This will not only keep your tension level at a fever pitch but could leave you in tears to walk home with. It's a complete realistic view at the spy game that stands as one of the best films of the year and a performance for the ages from Benedict Cumberbatch. A captivating achievement that I'll likely remember for some time.");
		System.out.println(result);
	}
	
	@Test
	public void testSubjectivity2(){
		String result = new IMDbSentimentSubjectivityClassifier().classify("I'll make my review short. Go see this. It has all the elements of a great movie from the acting to the score by Alexandre Desplat. Excellent script and delivery. It was well received at TIFF 14 where it was named People's Choice. The cast members really gelled on screen. It was well edited because the pace of the movie did not lag at any time. When watching this you will quickly realize that these actors (Benedict Cumberbatch, Keira Knightley, Matthew Goode, Mark Strong, Allen Leech) will be around for a long time entertaining us. If you like A Beautiful Mind or even Good Will Hunting, you will enjoy this. It comes out in November 2014. Don't miss it. This one is Oscar worthy.");
		System.out.println(result);
	}
	
	@Test
	public void testSubjectivity3(){
		String result = new IMDbSentimentSubjectivityClassifier().classify("In commenting on the film Benedict Cumberbatch has said there was no need to depict Turing's homosexuality as it was \"so obvious.\" I couldn't disagree more. It is one of the fundamental reasons why this man died before he was able to give so much more to society and find happiness for himself (perhaps in reverse order.) He helped end the war and the state, in return, chemically castrated him and cast him in to a pit. The film does NOT depict this on any acceptable level and I cannot be the only person to take this view, surely? It gets 4/10 for less than half a story. What is there is well acted. What isn't is a wholly different and more compelling film. Please somebody (not Hollywood) make it......");
		System.out.println(result);
	}

}
