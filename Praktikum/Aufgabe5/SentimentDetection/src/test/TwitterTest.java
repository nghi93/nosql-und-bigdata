package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sentiment.TwitterSentimentClassifier;

public class TwitterTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTwitter() {
		TwitterSentimentClassifier twitter = new TwitterSentimentClassifier();
		System.out.println(twitter.classify("@ImitationGame @WeinsteinFilms I still get goosebumps every time I see that scene when they crack the code for the first time. Brilliant!"));
	}

	@Test
	public void testTwitter2() {
		TwitterSentimentClassifier twitter = new TwitterSentimentClassifier();
		System.out.println(twitter.classify("I haven't been to the cinema in years! Today that will change! #TheImitationGame"));
	}
	
	@Test
	public void testTwitter3() {
		TwitterSentimentClassifier twitter = new TwitterSentimentClassifier();
		System.out.println(twitter.classify("Just watched The Imitation Game. Awesome, moving and accurate movie about the genius of Alan Turing http://theimitationgamemovie.com "));
	}
	
	@Test
	public void testTwitter4() {
		TwitterSentimentClassifier twitter = new TwitterSentimentClassifier();
		System.out.println(twitter.classify("Check out The Imitation Game @moviefone http://t.co/TTNR6xm42k"));
	}
}
