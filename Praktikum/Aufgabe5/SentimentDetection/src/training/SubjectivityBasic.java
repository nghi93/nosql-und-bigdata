package training;

import com.aliasi.util.Files;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.JointClassifier;
import com.aliasi.classify.JointClassifierEvaluator;

import com.aliasi.lm.LanguageModel;
import com.aliasi.lm.NGramProcessLM;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.io.IOException;


public class SubjectivityBasic {

    File mPolarityDir;
    String[] mCategories;
    DynamicLMClassifier<NGramProcessLM> mClassifier;

    SubjectivityBasic(String[] args) {
        System.out.println("\nBASIC SUBJECTIVITY DEMO");
        mPolarityDir = new File(args[0]);
        System.out.println("\nData Directory=" + mPolarityDir);
        mCategories = new String[] { "plot", "quote" };
        int nGram = 8;
        mClassifier = 
            DynamicLMClassifier
            .createNGramProcess(mCategories,nGram);
    }

    void run() throws ClassNotFoundException, IOException {
        train();
        evaluate();
    }

    void train() throws IOException {
        int numTrainingChars = 0;
        System.out.println("\nTraining.");
        for (int i = 0; i < mCategories.length; ++i) {
            String category = mCategories[i];
            Classification classification
                = new Classification(category);
            File file = new File(mPolarityDir,
                                 mCategories[i] + ".tok.gt9.5000");
            String data = Files.readFromFile(file,"ISO-8859-1");
            String[] sentences = data.split("\n");
            System.out.println("# Sentences " + category + "=" + sentences.length);
            int numTraining = (sentences.length * 9) / 10;
            for (int j = 0; j < numTraining; ++j) {
                String sentence = sentences[j];
                numTrainingChars += sentence.length();
                Classified<CharSequence> classified
                    = new Classified<CharSequence>(sentence,classification);
                mClassifier.handle(classified);
            }
        }
        
        System.out.println("\nCompiling.\n  Model file=subjectivity.model");
        FileOutputStream fileOut = new FileOutputStream("subjectivity.model");
        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
        mClassifier.compileTo(objOut);
        objOut.close();

        System.out.println("  # Training Cases=" + 9000);
        System.out.println("  # Training Chars=" + numTrainingChars);
    }

    void evaluate() throws IOException {
        // classifier hasn't been compiled, so it'll be slower
        boolean storeInputs = false;
        JointClassifierEvaluator<CharSequence> evaluator
            = new JointClassifierEvaluator<CharSequence>(mClassifier, mCategories,storeInputs);
        System.out.println("\nEvaluating.");
        for (int i = 0; i < mCategories.length; ++i) {
            String category = mCategories[i];
            Classification classification
                = new Classification(category);
            File file = new File(mPolarityDir,
                                 mCategories[i] + ".tok.gt9.5000");
            String data = Files.readFromFile(file,"ISO-8859-1");
            String[] sentences = "Quentin Tarantino, one of the most iconic directors of the 21st (and late 20th) century, why? Simple. Because of masterpieces like this. Tarantino defies the laws of film, he shoots them in his own way, however he wants. Tarantino has always focused upon the action thriller genre from Reservoir Dogs up until Inglourious Basterds. However, Django Unchained is Tarantino's first look at the Western genre, his first attempt at it and he executed it beautifully. The scenes were shot perfectly alongside an amazing soundtrack as well as his own small cameo.Django Unchained tells the story of Django (Jamie Foxx), a slave who is soon picked up by bounty hunter Dr King Shultz (Christoph Waltz). The story follows on as Shultz takes on Django as his \"deputy\" during their tasks of bounty hunting, in return Shultz says that after winter he will help find Django's lost wife, Broomhilda. This takes them to a huge plantation in Mississippi owned by Calvin Candie (Leonardo DiCaprio), from here they plan up a scheme on how to get away with Broombilda.The cast boast out amazing performances, particularly Christoph Waltz (also famous for his previous collaboration with Tarantino on Inglourious Bastards as Colonel Landa). Both Foxx and DiCaprio's performance are both equally amazing. All three are able to add some light-hearted humour in the mix to make sure it doesn't stay too serious, as well as having comic actor Jonah Hill play a member of the KKK.There's a reason the film has been nominated for 5 Oscars.".split("\n");
            int numTraining = (sentences.length * 9) / 10;
            for (int j = numTraining; j < sentences.length; ++j) {
                Classified<CharSequence> classified
                    = new Classified<CharSequence>(sentences[j],classification);
                evaluator.handle(classified);
            }
        }
        System.out.println();
        System.out.println(evaluator.toString());
    }

    public static void main(String[] args) {
        try {
            new SubjectivityBasic(args).run();
        } catch (Throwable t) {
            System.out.println("Thrown: " + t);
            t.printStackTrace(System.out);
        }
    }

}

