package mapreduce;

import geocoding.GeocodingUtil;

import java.io.IOException;

import kibana.KibanaAdapter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import sentiment.TwitterSentimentClassifier;

public class TwitterSentimentCount extends Configured implements Tool{	

	public static void main(String[] args) throws Exception{
		Configuration conf = new Configuration();
		
		 int res = ToolRunner.run(conf, new mapreduce.TwitterSentimentCount(), args);
         System.exit(res);
	}
	
	public int run(String[] args) throws Exception{
		
//		Configuration config = HBaseConfiguration.create();
		//Configuration config = getConf();
		//Job job = new Job(config, "ExampleRead");
		Job job = new Job(getConf());
		job.addFileToClassPath(new Path("json_simple-1.1.jar")); 
		job.addFileToClassPath(new Path("SentimentDetection.jar"));
		job.addFileToClassPath(new Path("lingpipe-4.1.0.jar"));
		
		job.setJarByClass(TwitterSentimentCount.class);     // class that contains mapper

		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes("Twitter"));
		scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
		scan.setCacheBlocks(false);  // don't set to true for MR jobs
		// set other scan attrs
		
		TableMapReduceUtil.initTableMapperJob(
		  "Meow",        // input HBase table name
		  scan,             // Scan instance to control CF and attribute selection
		  MyMapper.class,   // mapper
		  null,             // mapper output key
		  null,             // mapper output value
		  job);
		job.setOutputFormatClass(NullOutputFormat.class);   // because we aren't emitting anything from mapper

		boolean b = job.waitForCompletion(true);
//		if (!b) {
//		  throw new IOException("error with job!");
//		}
		return job.waitForCompletion(true) ? 0 : 1;

	}
	
	public static class MyMapper extends TableMapper<Text, Text> {
		private final byte[] CF = Bytes.toBytes("Twitter");
		private KibanaAdapter kibana = new KibanaAdapter("http://localhost:9200/");
		  public void map(ImmutableBytesWritable row, Result value, Context context) throws InterruptedException, IOException {
			  String created = Bytes.toString(value.getValue(CF, Bytes.toBytes("created")));
			  String movie = Bytes.toString(value.getValue(CF, Bytes.toBytes("movie")));
			  String tweet = Bytes.toString(value.getValue(CF, Bytes.toBytes("tweet")));
			  String country = "";
			  if(value.getValue(CF, Bytes.toBytes("latitude")) != null && value.getValue(CF, Bytes.toBytes("longitude")) != null){
				  double latitude = Bytes.toDouble(value.getValue(CF, Bytes.toBytes("latitude")));
				  double longitude = Bytes.toDouble(value.getValue(CF,  Bytes.toBytes("longitude")));
				  country = GeocodingUtil.latlngToCountry(latitude, longitude);
			  }
			  
			  String tweet_id = Bytes.toString(row.get());
			  TwitterSentimentClassifier twitter = new TwitterSentimentClassifier();
			  String result = twitter.classify(tweet);
			  String json = "{\"country\":\""+country+"\", \"result\":\""+result+"\", \"time\":\""+created+"\"}";
			  System.out.println(json);
			  System.out.println(tweet_id);
			  kibana.send("twitter", "tweet", String.valueOf(tweet_id), json);
		  }
	}   
}