package mapreduce;

import geocoding.GeocodingUtil;

import java.io.IOException;

import kibana.KibanaAdapter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import sentiment.IMDbSentimentPolarityClassifier;
import sentiment.TwitterSentimentClassifier;

public class IMDbSentimentCount extends Configured implements Tool{	

	public static void main(String[] args) throws Exception{
		Configuration conf = new Configuration();
		
		 int res = ToolRunner.run(conf, new mapreduce.IMDbSentimentCount(), args);
         System.exit(res);
	}
	
	public int run(String[] args) throws Exception{
		
//		Configuration config = HBaseConfiguration.create();
		//Configuration config = getConf();
		//Job job = new Job(config, "ExampleRead");
		Job job = new Job(getConf());
		job.addFileToClassPath(new Path("json_simple-1.1.jar")); 
		job.addFileToClassPath(new Path("SentimentDetection.jar"));
		job.addFileToClassPath(new Path("lingpipe-4.1.0.jar"));
		
		job.setJarByClass(IMDbSentimentCount.class);     // class that contains mapper

		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes("IMDb"));
		scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
		scan.setCacheBlocks(false);  // don't set to true for MR jobs
		// set other scan attrs
		
		TableMapReduceUtil.initTableMapperJob(
		  "Meow",        // input HBase table name
		  scan,             // Scan instance to control CF and attribute selection
		  MyMapper.class,   // mapper
		  null,             // mapper output key
		  null,             // mapper output value
		  job);
		job.setOutputFormatClass(NullOutputFormat.class);   // because we aren't emitting anything from mapper

		boolean b = job.waitForCompletion(true);
//		if (!b) {
//		  throw new IOException("error with job!");
//		}
		return job.waitForCompletion(true) ? 0 : 1;

	}
	
	public static class MyMapper extends TableMapper<Text, Text> {
		private final byte[] CF = Bytes.toBytes("IMDb");
		private KibanaAdapter kibana = new KibanaAdapter("http://localhost:9200/");
		  public void map(ImmutableBytesWritable row, Result value, Context context) throws InterruptedException, IOException {
			  boolean run = true;
			  int i = 0;
			  while(run){
				  if(value.getValue(CF, Bytes.toBytes(i+"created")) == null){
					  run = false;
					  break;
				  }
				  String created = Bytes.toString(value.getValue(CF, Bytes.toBytes(i+"created")));
				  String country = "";
				  if(value.getValue(CF, Bytes.toBytes(i+"location")) != null){
					  String location = Bytes.toString(value.getValue(CF, Bytes.toBytes(i+"location")));
					  location = location.replace("from ", "");
					  System.out.println("new location: "+location);
					  country = GeocodingUtil.placeToCountry(location);
				  }
				  String review = Bytes.toString(value.getValue(CF, Bytes.toBytes(i+"review")));
				  
				  String review_id = String.valueOf(i);
				  IMDbSentimentPolarityClassifier imdb = new IMDbSentimentPolarityClassifier();
				  String result = imdb.classify(review);
				  String json = "{\"country\":\""+country+"\", \"result\":\""+result+"\", \"time\":\""+created+"\"}";
				  System.out.println(json);
				  System.out.println(review_id);
				  kibana.send("imdb", "reviews", review_id, json);
				  i++;
			  }
		  }
	}   
}