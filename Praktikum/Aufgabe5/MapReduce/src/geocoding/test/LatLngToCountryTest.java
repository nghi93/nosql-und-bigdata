package geocoding.test;

import static org.junit.Assert.*;
import geocoding.GeocodingUtil;

import org.junit.Test;

public class LatLngToCountryTest {

	@Test
	public void testLatLongToCountry(){
		double lat = 40.704093;
		double lng = 60.193582;

		assertEquals("TM", GeocodingUtil.latlngToCountry(lat, lng));
	}
	
	@Test
	public void testPlaceToCountry(){
		assertEquals("US", GeocodingUtil.placeToCountry("New Jersey"));
		assertEquals("DE", GeocodingUtil.placeToCountry("Hamburg"));
		assertEquals("CA", GeocodingUtil.placeToCountry("Toronto,+Ontario"));
		assertEquals("GB", GeocodingUtil.placeToCountry("Worcester,+England"));
	}

}
