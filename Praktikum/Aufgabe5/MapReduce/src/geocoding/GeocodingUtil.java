package geocoding;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class GeocodingUtil {
	public static String latlngToCountry(double lat, double lng){
		String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=false";
		StringBuffer response = new StringBuffer();
		try {
			URL url2 = new URL(url);
			HttpURLConnection connection = (HttpURLConnection)url2.openConnection();
		     connection.setRequestMethod("GET");
		     connection.setUseCaches (false);
		      connection.setDoInput(true);
		      connection.setDoOutput(true);
		     DataOutputStream wr = new DataOutputStream (
	                  connection.getOutputStream ());
		      wr.flush ();
		      wr.close ();
		      
		    //Get Response    
		      InputStream is = connection.getInputStream();
		      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		      String line;
		      
		      while((line = rd.readLine()) != null) {
		        response.append(line);
		        response.append('\r');
		      }
		      rd.close();
		      
		      //System.out.println(response);
		      Object obj = JSONValue.parse(response.toString());
	          JSONObject map = (JSONObject)obj;
	          for(Object test : ((JSONArray)((JSONObject)((JSONArray)map.get("results")).get(0)).get("address_components"))){
	        	 JSONObject b = (JSONObject) JSONValue.parse(test.toString());
	        	 JSONArray a = (JSONArray)b.get("types");
	        	 if(a.contains("country") && a.contains("political")){
	        		return b.get("short_name").toString();
	        	 }
	          }
	          
	          
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(IndexOutOfBoundsException e){
			System.out.println("lat:"+lat);
			System.out.println("lng:"+lng);
			System.out.println("respond:"+response);
		}
		return "";
	}
	
	public static String placeToCountry(String place){
		StringBuffer response = new StringBuffer(); 
		try {
			String url = "http://maps.googleapis.com/maps/api/geocode/json?address="+URLEncoder.encode(place, "UTF-8");
			URL url2 = new URL(url);
			HttpURLConnection connection = (HttpURLConnection)url2.openConnection();
		     connection.setRequestMethod("GET");
		     connection.setUseCaches (false);
		      connection.setDoInput(true);
		      connection.setDoOutput(true);
		     DataOutputStream wr = new DataOutputStream (
	                  connection.getOutputStream ());
		      wr.flush ();
		      wr.close ();
		      
		    //Get Response    
		      InputStream is = connection.getInputStream();
		      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		      String line;
 
		      while((line = rd.readLine()) != null) {
		        response.append(line);
		        response.append('\r');
		      }
		      rd.close();
		      
		      //System.out.println(response);
		      JSONObject obj = (JSONObject)JSONValue.parse(response.toString());
		      for(Object test : (JSONArray)((JSONObject)((JSONArray)obj.get("results")).get(0)).get("address_components")){
		    	  JSONObject b = (JSONObject) JSONValue.parse(test.toString());
		    	  JSONArray a = (JSONArray)b.get("types");
		    	  if(a.contains("country") && a.contains("political")){
		    		  return b.get("short_name").toString();
		    	  }
		      }
		      
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IndexOutOfBoundsException e){
			System.out.println("place:"+place);
			System.out.println("response:"+response);
		}
		return "";
	}
	
}
