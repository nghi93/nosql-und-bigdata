package kibana;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class KibanaAdapter {
	private final String url;
	
	public KibanaAdapter(String url){
		this.url = url;
	}
	
	public void send(String index, String type, String id, String msg){
		HttpURLConnection httpCon;
		try {
			httpCon = (HttpURLConnection) new URL(url+index+"/"+type+"/"+id).openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestMethod("PUT");
			OutputStreamWriter out = new OutputStreamWriter(
			    httpCon.getOutputStream());
			out.write(msg);
			out.close();
			httpCon.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
