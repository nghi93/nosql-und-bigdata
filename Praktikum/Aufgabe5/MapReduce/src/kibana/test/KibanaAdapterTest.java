package kibana.test;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import kibana.KibanaAdapter;

import org.junit.Test;

public class KibanaAdapterTest {

	@Test
	public void testSend() {
		KibanaAdapter kibana = new KibanaAdapter("http://localhost:9200/");
		kibana.send("twitter", "tweet", "2", "{\"country\":\"PL\",\"result\":\"neu\",\"time\":\"2014-11-17 17:23:26\"}");
	}
	
	@Test
	public void testSend2() {
		KibanaAdapter kibana = new KibanaAdapter("http://localhost:9200/");
		kibana.send("imdb", "reviews", "test", "{\"country\":\"PL\",\"result\":\"neu\",\"time\":\"14 September 2014\"}");
	}
}
