﻿Produkt-Stimmungsanalyse über Soziale Netzwerke
Meow: Mood Evaluation Over tWitter
Mood Evaluation Over social netWork
Mood EvaluatiOn Website

Ziel:
- Stimmungsanalyse über ein Produkt über soziale Netzwerke
- dabei User mitanalysieren, z.B. Geschlecht, Alter, Ort
- Oberfläche: Graph mit zusammenhörigen Produkten, bei Klick Stimmungsanalyse durchführen
	Anzeige über Zeit, Region

Ziele zum 28.11.2014:
- Twitter API (Zugriff auf welche User/Daten) 
- Sentiment Detection (Java) http://alias-i.com/lingpipe/demos/tutorial/sentiment/read-me.html
- Datenspeicherung(Hbase)
- Programmiersprache (Java)


redis zum Speichern der Ergebnisse


Offene Punkte:
MapReduce zum Auswerten der Tweets?


Ansatz:
Da wir nicht genügend Speicher haben, crawlen wir uns eine angemessene Anzahl an Tweets zu einem bestimmtem Suchbegriff.
- Twitter API (begrenzte Anzahl)
- Web Crawler
- Streaming API

Anschließend analysieren wir die Daten über MapReduce für gewisse Zeiträume.
- map: Bewerte Tweet
- reduce bilde Durchschnitt

Die Ergebnisse werden über die Zeiträume in Redis gespeichert.
Auf einer Webseite werden die Ergebnisse über Zeiträume angezeigt.
s. Mock

---------------------------------------------------------------

Twitter-Crawler kommt an Tweets ran, eventuell auch mit Geodaten und User.
Problem: Beschränkung der Zugriffe über Zeit.
Lösung: einmaliges Absuchen einer bestimmten Anzahl zu bestimmmten Themen.

IMDB-Crawer: in Java, scannt User-Bewertung.

Probleme: wie sinnvoll in HBase abspeichern?
Eine Table, mit einer ColumnFamiliy Twitter und einer CF IMDB.
Key ist jeweils der Moviename, Spaltenbezeichner ist einfach eine Zahl und Value sind dann die Posts der User.

							Table
						---------------------------------------
						| CF "IMDb"        | CF "Twitter"     |
Key:					---------------------------------------
"The Imitation Game" -> | qualifier : value| qualifier : value|
						---------------------------------------

Sentiment Detection:
schlechte Detection, könnten aber trainieren (z.B. IMDb Reviews nach Bewertungen klassifizieren)
Reviews eher negativ als reine Bewertungen						
					
MapReduce-Problem: wie Daten aus HBase bekommen?
-----------------------------------------------------------------------------------------
Anzahl der Tweets, die ausgewertet wurden.
Anzahl der Reviews, die ausgewertet wurden.
Positive, Neutrale, Negative Tweets zu einem Film insgesamt.
Positive, Negative Tweets zu einem Film insgesamt.
Bewertung von Tweets über Zeitraum.
Bewertung von Reviews über Zeitraum.
Verteilung der Tweets über Welt (Positiv, neutral, negativ)
Verteilung der Tweets über Welt (Positiv, negativ)
Liste mit Filmen?
-------------------------------------------------------------------------

export HADOOP_CLASSPATH=/home/nosql/Dokumente/Uni/nosql-und-bigdata/Praktikum/Aufgabe5/MapReduce/lib/json_simple-1.1.jar,/home/nosql/Dokumente/Uni/nosql-und-bigdata/Praktikum/Aufgabe5/SentimentDetection.jar

./hadoop jar /home/nosql/Dokumente/Uni/nosql-und-bigdata/Praktikum/Aufgabe5/Twitter.jar mapreduce.TwitterSentimentCount
./hadoop jar /home/nosql/Dokumente/Uni/nosql-und-bigdata/Praktikum/Aufgabe5/IMDb.jar mapreduce.IMDbSentimentCount

third party jars ins hdfs laden
twitter.model, polarity.model in Twitter_lib
---------------
YYYY -> yyyy

-----------------------
