import redis
import json

r = redis.StrictRedis(host='localhost', port=6379, db=0)
FILE = "plz.data"
MAP = "cityplzmap"

f = open(FILE, 'r')
for line in f:
    temp = json.loads(line)
    
    #json.loads causes single quotes in json-objects
    #json.dumps corrects ' -> "   
    r.set(temp["_id"], json.dumps(temp))
    r.hset(MAP, temp["city"], temp["_id"])

