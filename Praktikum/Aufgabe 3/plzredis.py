import sys
import redis
import json


r = redis.StrictRedis(host='localhost', port=6379, db=0)
MAP = "cityplzmap"
ENCODING = 'utf-8'
while(True):
    var = input("Please enter ZIP code or city: ")

    if(var.isdigit()):
        result = r.get(var)
        temp = json.loads(result.decode(ENCODING))
        city = temp["city"]
        location = temp["loc"]
        print("ZIP:"+var, "City:"+city, "Location:", location)
    else:
        result = r.hget(MAP, var)
        zip = result.decode(ENCODING)
        print("City:"+var, "ZIP:"+zip)
