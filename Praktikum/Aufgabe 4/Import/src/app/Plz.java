package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map.Entry;
import java.util.NavigableMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.json.simple.JSONObject;

public class Plz {
	private final static String TABLENAME = "plz";
	private final static String FAMILYNAME = "plz";
	
	private static boolean isDigit(String s){
		return s.matches("^\\d+$");
	}
	
	public static void main(String[] args) {
		Configuration config = HBaseConfiguration.create();
		InputStreamReader isr = new InputStreamReader ( System.in );
		BufferedReader br = new BufferedReader ( isr );
		String s = null;
		try {
			System.out.println("Please enter ZIP code or city: ");
		   while ( (s = br.readLine ()) != null ) {
		      
		      if(isDigit(s)){
		    	  findByZIP(config, s);
		      }
		      else{
		    	  findByName(config, s);
		      }
		      System.out.println("Please enter ZIP code or city: ");
		   }
		}
		catch ( IOException ioe ) {
		   // won't happen too often from the keyboard
		}
	}

	private static void findByZIP(Configuration config, String zip){
		try {
			HTable table = new HTable(config, TABLENAME);
			Get get = new Get(Bytes.toBytes("_id:"+zip)); 
			long start = System.nanoTime();
			Result result = table.get(get);
			long end = System.nanoTime();
			long time = (end-start)/1000000;
			System.out.println("time: "+time+"ms");
			String city = new String(result.getValue(Bytes.toBytes(FAMILYNAME), Bytes.toBytes("city")));
			String state = new String(result.getValue(Bytes.toBytes("plz"), Bytes.toBytes("state")));
			System.out.println("city: "+city+", state :"+state);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void findByName(Configuration config, String name){
		try {
			HTable table = new HTable(config, TABLENAME);
			Scan scan = new Scan(); 
			scan.setFilter(new SingleColumnValueFilter(Bytes.toBytes(FAMILYNAME), Bytes.toBytes("city"), CompareOp.EQUAL, Bytes.toBytes(name)));
			long start = System.nanoTime();
			ResultScanner rs = table.getScanner(scan);
			long end = System.nanoTime();
			long time = (end-start)/1000000;
			System.out.println("time: "+time+"ms");
            for (Result r : rs) { 
                System.out.println("key:" + new String(r.getRow()));  
            }  
            rs.close();  
            table.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
