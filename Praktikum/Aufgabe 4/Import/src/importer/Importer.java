package importer;



import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.*;

public class Importer {
	private final String PATH = "/home/nosql/Dokumente/Uni/nosql-und-bigdata/Praktikum/Aufgabe 4/plz.data";
	private final String TABLENAME = "plz";
	private final String FAMILYNAME = "plz";
	private final String FAMILYNAME_FUSSBALL = "Fussball";
	
	public static void main(String[] args) {
		Configuration config = HBaseConfiguration.create();
		Importer importer = new Importer();
		importer.deleteSchema(config);
		importer.createSchema(config);
		importer.importJson(config);
	}
	
	public void deleteSchema(Configuration config){
		try {
			HBaseAdmin admin = new HBaseAdmin(config);
			admin.disableTable(TABLENAME);
			admin.deleteTable(TABLENAME);
			admin.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSchema(Configuration config){
		
		HBaseAdmin admin;
		try {
			admin = new HBaseAdmin(config);

			HTableDescriptor table = new HTableDescriptor(TableName.valueOf(TABLENAME));
			table.addFamily(new HColumnDescriptor(FAMILYNAME));
			table.addFamily(new HColumnDescriptor(FAMILYNAME_FUSSBALL));
			admin.createTable(table);
			admin.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void importJson(Configuration config){
		
		String key = "_id";
		String col1 = "city";
		String col2 = "latitude";
		String col3 = "longitude";
		String col4 = "pop";
		String col5 = "state";
		String col6 = "fussball";
		List<Put> list;
		try {
			HTable table = new HTable(config, TABLENAME);
			BufferedReader br = new BufferedReader(new FileReader(PATH));

	        String line = br.readLine();

	        while (line != null) {            
	            Object obj = JSONValue.parse(line);
	            JSONObject map = (JSONObject)obj;
	            JSONArray array = (JSONArray)map.get("loc");
	            
	            Put p = new Put(Bytes.toBytes(key+":"+map.get(key)));
	            p.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(col1), Bytes.toBytes(map.get(col1).toString()));
	            
	            Put p2 = new Put(Bytes.toBytes(key+":"+map.get(key)));
	            p2.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(col2), Bytes.toBytes(array.get(0).toString()));
	            
	            Put p3 = new Put(Bytes.toBytes(key+":"+map.get(key)));
	            p3.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(col3), Bytes.toBytes(array.get(1).toString()));
	            
	            Put p4 = new Put(Bytes.toBytes(key+":"+map.get(key)));
	            p4.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(col4), Bytes.toBytes(map.get(col4).toString()));
	            
	            Put p5 = new Put(Bytes.toBytes(key+":"+map.get(key)));
	            p5.add(Bytes.toBytes(FAMILYNAME), Bytes.toBytes(col5), Bytes.toBytes(map.get(col5).toString()));
	            list = new ArrayList<Put>(Arrays.asList(p, p2, p3, p4, p5));
	            if(map.get(col1).toString().equals("HAMBURG") || map.get(col1).toString().equals("BREMEN")){
	            	Put p6 = new Put(Bytes.toBytes(key+":"+map.get(key)));
	            	p6.add(Bytes.toBytes(FAMILYNAME_FUSSBALL), Bytes.toBytes(col6), Bytes.toBytes("ja"));
	            	list.add(p6);
	            }
	            
	            table.put(list);
	            line = br.readLine();
	        }
	        br.close();
	    }
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
