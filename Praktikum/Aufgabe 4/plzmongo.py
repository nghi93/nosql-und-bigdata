import sys
from pymongo import MongoClient
import json
import time

client = MongoClient('localhost', 27017)
db = client.nosql

def findByZIP(db, zip):
        zip = str(zip)
        #print("scanned: "+str(db.plz.find({ "_id" : zip}).explain()["nscanned"]))
        start = time.time()
        result = db.plz.find({ "_id" : zip})
        end = time.time()
        elapsed_time = end-start
        print("time: "+str(elapsed_time))
        return result
def findByName(db, name):
        name = str(name)
        print("scanned: "+str(db.plz.find({"city" : name}).explain()["nscanned"]))
        start = time.time()
        result = db.plz.find({"city" : name})
        end = time.time()
        elapsed_time = end-start
        print("time: "+str(elapsed_time))
        return result

while(True):
    var = input("Please enter ZIP code or city: ")

    if(var.isdigit()):
        result = findByZIP(db, var)
        for temp in result:
                city = temp["city"]
                state = temp["state"]
                print("ZIP:"+var, "City:"+city, "State:", state)
    else:
        result = findByName(db, var)
        for temp in result:
                zip = temp["_id"]
                print("City:"+var, "ZIP:"+zip)
