import sys
import redis
import json
import time

r = redis.StrictRedis(host='localhost', port=6379, db=0)
MAP = "cityplzmap"
ENCODING = 'utf-8'
while(True):
    var = input("Please enter ZIP code or city: ")

    if(var.isdigit()):
        start_time = time.time()
        result = r.get(var)
        elapsed_time = time.time() - start_time
        print("time :"+str(elapsed_time));
        temp = json.loads(result.decode(ENCODING))
        city = temp["city"]
        location = temp["loc"]
        print("ZIP:"+var, "City:"+city, "Location:", location)
    else:
        start_time = time.time()
        result = r.hget(MAP, var)
        elapsed_time = time.time() - start_time
        print("time :"+str(elapsed_time));
        zip = result.decode(ENCODING)
        print("City:"+var, "ZIP:"+zip)
